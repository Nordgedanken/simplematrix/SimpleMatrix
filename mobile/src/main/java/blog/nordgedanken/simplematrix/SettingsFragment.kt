package blog.nordgedanken.simplematrix

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import androidx.preference.PreferenceFragmentCompat
import com.orhanobut.logger.Logger

/**
 * Created by MTRNord on 03.01.2019.
 */
class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        findPreference("room_bg_image").setOnPreferenceClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            val PICK_IMAGE = 1
            startActivityForResult(intent, PICK_IMAGE)
            true
        }
    }

    @SuppressLint("ApplySharedPref")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            val selectedImage = data?.data
            preferenceManager.sharedPreferences.edit().putString("room_bg_image_url", selectedImage?.toString()!!).commit()
            Logger.d(preferenceManager.sharedPreferences.getString("room_bg_image_url", "")!!)
        }
    }

}