package blog.nordgedanken.simplematrix.utils

import android.accounts.*
import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.os.Handler
import java.io.IOException
import java.util.concurrent.TimeUnit

/*
  Copyright 2018 Michigan Software Labs, LLC
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/


/*
 * In API 22 (LOLLIPOP_MR1),
 * AccountManager.removeAccount(Account, Activity, AccountManagerCallback<Bundle>, Handler) was added, and
 * AccountManager.removeAccount(Account, AccountManagerCallback<Boolean>, Handler) was deprecated.
 *
 * To resolve this when running on a pre-22 device, call the old .removeAccount(), get the Boolean result,
 * put it in a Bundle using key AccountManager.KEY_BOOLEAN_RESULT,
 * then call AccountManagerCallback<Bundle> callback.run() with a new AccountManagerFuture<Bundle>
 * that returns this Bundle.
 */
fun removeAccount(
    account: Account,
    activity: Activity?,
    callback: AccountManagerCallback<Bundle>?,
    handler: Handler?,
    accountManager: AccountManager
) {
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
    accountManager.removeAccount(account, activity, callback, handler)
  } else {
    @Suppress("DEPRECATION")
    accountManager.removeAccount(
        account,
        AccountManagerCallback<Boolean> { future ->
          if (callback == null) {
            return@AccountManagerCallback
          }

          callback.run(object : AccountManagerFuture<Bundle> {
            override fun cancel(mayInterruptIfRunning: Boolean): Boolean {
              return future.cancel(mayInterruptIfRunning)
            }

            override fun isCancelled(): Boolean {
              return future.isCancelled
            }

            override fun isDone(): Boolean {
              return future.isDone
            }

            @Throws(OperationCanceledException::class, IOException::class, AuthenticatorException::class)
            override fun getResult(): Bundle {
              val result = Bundle()
              result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, future.result)
              return result
            }

            @Throws(OperationCanceledException::class, IOException::class, AuthenticatorException::class)
            override fun getResult(timeout: Long, unit: TimeUnit): Bundle {
              val result = Bundle()
              result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, future.getResult(timeout, unit))
              return result
            }
          })
        },
        handler
    )
  }
}