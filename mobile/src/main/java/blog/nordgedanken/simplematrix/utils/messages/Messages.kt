package blog.nordgedanken.simplematrix.utils.messages

import android.content.Context
import android.text.Spanned
import android.text.style.ImageSpan
import androidx.core.text.HtmlCompat
import blog.nordgedanken.simplematrix.R
import com.binaryfork.spanny.Spanny
import com.google.android.material.chip.ChipDrawable
import org.jsoup.Jsoup
import org.jsoup.helper.StringUtil
import org.jsoup.nodes.Element
import org.jsoup.nodes.Node
import org.jsoup.parser.Parser
import org.koin.standalone.KoinComponent

/**
 * Created by MTRNord on 03.03.2019.
 */
class Messages(private val context: Context) : KoinComponent {
    fun fromHTML(s: String): Spanned {
        val doc = Jsoup.parse(s)

        return parseBody(doc.body())
    }

    private fun parseBody(body: Element): Spanned {
        val matrixToLinks = body.select("a[href^=\"https://matrix.to/#/\"]")
        val spanny = Spanny()
        if (matrixToLinks.isNotEmpty()) {
            for (link in matrixToLinks) {
                val previous = link.previousSibling()
                val next = link.nextSibling()

                if (previous != null) {
                    val previousE = Jsoup.parse(previous.html(), "", Parser.xmlParser())
                    spanny.append(HtmlCompat.fromHtml(previousE.text(), HtmlCompat.FROM_HTML_MODE_COMPACT))
                }

                val chip = ChipDrawable.createFromResource(context, R.xml.chip)
                chip.setText(link.text())
                chip.elevation = 1.0F
                chip.setBounds(0, 0, chip.intrinsicWidth, chip.intrinsicHeight/2+4)
                val span = ImageSpan(chip)

                spanny.append("", span)

                if (next != null) {
                    val nextE = Jsoup.parse(next.html(), "", Parser.xmlParser())
                    spanny.append(HtmlCompat.fromHtml(nextE.text(), HtmlCompat.FROM_HTML_MODE_COMPACT))
                }
            }
        } else {
            spanny.append(HtmlCompat.fromHtml(body.text(), HtmlCompat.FROM_HTML_MODE_COMPACT))
        }

        return spanny
    }
}

fun Node.html(): String {
    val accum = StringUtil.stringBuilder()
    html(accum)
    return accum.toString()
}