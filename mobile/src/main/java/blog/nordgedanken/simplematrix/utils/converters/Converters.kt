/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.utils.converters

import androidx.room.TypeConverter
import blog.nordgedanken.simplematrix.data.view.User
import com.google.gson.GsonBuilder
import java.util.*

/**
 * Created by MTRNord on 01.09.2018.
 */
class Converters {
    private val builder = GsonBuilder()
    private val gjson = builder.create()

    // Type Date
    @TypeConverter
    fun fromTimestamp(value: Long?): Date {
        return Date(value!!)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long {
        return date!!.time
    }

    //////////////////////////
    // Type List<String>
    @TypeConverter
    fun toString(value: List<String>?): String {
        return gjson.toJson(value)
    }

    @TypeConverter
    fun toStringList(value: String): List<String>? {
        val objects = gjson.fromJson(value, Array<String>::class.java)
        return objects.toList()
    }

    @TypeConverter
    fun membershipEnumToTnt(value: User.Membership?) = value?.toInt()

    @TypeConverter
    fun intToMembershipEnum(value: Int?) = value?.toEnum<User.Membership>()
}

@Suppress("NOTHING_TO_INLINE")
private inline fun <T : Enum<T>> T.toInt(): Int = this.ordinal

private inline fun <reified T : Enum<T>> Int.toEnum(): T = enumValues<T>()[this]