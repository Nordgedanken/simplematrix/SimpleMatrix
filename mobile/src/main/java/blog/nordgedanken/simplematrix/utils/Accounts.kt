package blog.nordgedanken.simplematrix.utils

import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AccountManagerCallback
import android.accounts.AccountManagerFuture
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat.startActivity
import blog.nordgedanken.matrix_android_sdk.Matrix
import blog.nordgedanken.simplematrix.BuildConfig
import blog.nordgedanken.simplematrix.MainActivity
import blog.nordgedanken.simplematrix.accounts.MatrixAccountAuthenticator
import blog.nordgedanken.simplematrix.fcm.FCMHelper
import com.orhanobut.logger.Logger
import io.kamax.matrix.MatrixID
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import org.jetbrains.anko.doAsync
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

/**
 * Created by MTRNord on 16.03.2019.
 */
class Accounts(val activity: Activity, val target: Intent) : KoinComponent {


    /**
     * Account manager is needed to work with Android Accounts
     */
    val am: AccountManager by inject()

    /**
     * Holds any available Accounts
     */
    val accounts: Array<Account> by inject()

    /**
     * Checks the [blog.nordgedanken.simplematrix.utils.Accounts.accounts] Array for existing Accounts
     * of the type org.matrix.
     *
     * If No accounts are present in the Array aka. it is Empty it launches a new Login Request.
     * Else it uses the first account in the Array to login with.
     *
     * TODO Make this Multi Account Capable
     */
    fun checkAccounts() {
        if (accounts.isEmpty()) {
            MainActivity.firstLogin = true
            Sentry.getContext().recordBreadcrumb(
                    BreadcrumbBuilder().setMessage("First login").build()
            )
            am.addAccount("org.matrix", "token", null, Bundle(), activity, OnTokenAcquired(), null)
        } else {
            // TODO rework to show available Accounts to the User and let him choose
            am.getAuthToken(accounts[0], MatrixAccountAuthenticator.TOKEN_TYPE, null, false, OnTokenAcquired(), null)
        }
    }

    private inner class OnTokenAcquired : AccountManagerCallback<Bundle> {

        override fun run(result: AccountManagerFuture<Bundle>?) {
            Logger.d("login")
            if (result !== null) {
                val bundle: Bundle? = try {
                    result.result
                } catch (e: Exception) {
                    Sentry.capture(e)
                    null
                }

                val launch = bundle?.get(AccountManager.KEY_INTENT) as? Intent
                if (launch != null) {
                    startActivityForResult(activity, launch, 0, null)
                } else {
                    doAsync {
                        if (!MainActivity.firstLogin) {
                            val token = bundle
                                    ?.getString(AccountManager.KEY_AUTHTOKEN)

                            val id = MatrixID.Builder(am.getUserData(accounts[0], "idString")).acceptable()

                            Matrix(id, am.getUserData(accounts[0], "baseUrl"), am.getUserData(accounts[0], "domain")!!, token!!) {
                                if (BuildConfig.ALLOW_FCM_USE) {
                                    FCMHelper().init(activity)
                                }

                                startActivity(activity, target, null)
                                activity.finish()
                            }
                        } else {
                            startActivity(activity, target, null)
                            activity.finish()
                        }
                    }
                }
            } else {
                throw Exception("Login failed because AccountManagerFuture is null")
            }
            Logger.d("login done")
        }
    }
}