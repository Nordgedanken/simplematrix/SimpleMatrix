package blog.nordgedanken.simplematrix.utils

import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import blog.nordgedanken.simplematrix.MainActivity
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import java.security.MessageDigest

/**
 * Created by MTRNord on 26.02.2019.
 */
fun forceNewLogin(context: Context) {
    RoomsActivity.syncStatus.postValue(context.getString(R.string.error, "Invalid access_token"))
    val am = AccountManager.get(context)
    val accounts = am?.getAccountsByType("org.matrix")
    // TODO fix when using multiple accounts
    removeAccount(account = accounts!![0], activity = null, handler = null, callback = null, accountManager = am)

    // Let the User login again
    val mainActivityIntent = Intent(context, MainActivity::class.java)
    ContextCompat.startActivity(context, mainActivityIntent, null)
}

fun String.sha512(): String {
    return this.hashWithAlgorithm("SHA-512")
}

private fun String.hashWithAlgorithm(algorithm: String): String {
    val digest = MessageDigest.getInstance(algorithm)
    val bytes = digest.digest(this.toByteArray(Charsets.UTF_8))
    return bytes.fold("") { str, it -> str + "%02x".format(it) }
}