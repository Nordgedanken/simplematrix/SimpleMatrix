/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Created by Jaison on 25/10/16.
 */

class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
  private val mFragmentList = mutableMapOf<Int, Fragment>()
  private val mFragmentTitleList = mutableMapOf<Int, String>()

  override fun getItem(position: Int): Fragment {
    return mFragmentList[position]!!
  }

  override fun getCount(): Int {
    return mFragmentList.size
  }

  fun addFragment(position: Int, fragment: Fragment, title: String) {
    mFragmentList[position] = fragment
    mFragmentTitleList[position] = title
  }

  override fun getPageTitle(position: Int): CharSequence? {
    return mFragmentTitleList[position]
  }

  override fun getItemPosition(`object`: Any): Int {
    return super.getItemPosition(`object`)
  }


}