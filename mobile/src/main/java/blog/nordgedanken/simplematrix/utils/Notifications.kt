/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.GlideApp
import blog.nordgedanken.simplematrix.NOTIFICATION_DELETED_ACTION
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.chatView.ChatRoom
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.view.MessageFull
import blog.nordgedanken.simplematrix.data.view.Notification
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import com.orhanobut.logger.Logger
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.util.*


/**
 * Created by MTRNord on 09.08.2018.
 */
class Notification : KoinComponent {
    val DirectChatsChannelID = "directChat"
    val FavChatsChannelID = "favChat"
    val ChatsChannelID = "Chat"
    val LISTENING_FOR_EVENTS_NOTIFICATION = "LISTEN_FOR_EVENTS_NOTIFICATION"
    private val mNotificationManager: NotificationManagerCompat by inject()
    private val account: Account by inject()
    private val db: AppDatabase by inject()

    fun addMessageToRoomNotification(
            context: Context,
            channelID: String,
            priority: String,
            message: MessageFull
    ) {
        val id = Date().time.toInt()
        val room = db.getRoomByID(message.message?.roomID!!)!!
        val ownUser = db.getUserByMXIDRoomIDFromCache(
                account.client?.context?.user?.get()?.id!!,
                message.message?.roomID!!
        )

        if (ownUser != null) {
            val style = NotificationCompat.MessagingStyle(ownUser.getPerson())
                    .setConversationTitle(room.getDialogName())

            if (channelID != DirectChatsChannelID) {
                style.isGroupConversation = true
            }

            // FIXME use sharedPreference as db is too slow
            val oldNotification = db.notificationDao().notificationByRoomID(message.message?.roomID!!)
            if (oldNotification != null) {
                val messages = db.messageDao().loadAllByIds(oldNotification.messageIDs)
                for (messageL in messages) {

                    style.addMessage(NotificationCompat.MessagingStyle.Message(
                            messageL.message?.text,
                            messageL.message?.createdAt?.time!!,
                            messageL.getUser().getPerson()
                    ))
                }


                style.addMessage(NotificationCompat.MessagingStyle.Message(
                        message.message?.text,
                        message.message?.createdAt?.time!!,
                        message.getUser().getPerson()
                ))
                val notification = prepareNotification(context, style, channelID, room, priority)
                        .build()

                mNotificationManager.notify(oldNotification.id, notification)

                val newList = mutableListOf<String>()
                newList.addAll(oldNotification.messageIDs)
                newList.add(message.message?.id!!)
                oldNotification.messageIDs = newList
                db.notificationDao().updateOne(oldNotification)
            } else {
                style.addMessage(NotificationCompat.MessagingStyle.Message(
                        message.message?.text,
                        message.message?.createdAt?.time!!,
                        message.getUser().getPerson()
                ))
                val notification = prepareNotification(context, style, channelID, room, priority)
                        .build()

                mNotificationManager.notify(id, notification)
                val notificationDB = Notification(id, message.message?.roomID!!, mutableListOf(message.message?.id!!))
                db.notificationDao().insertOne(notificationDB)
            }
        }
    }

    private fun prepareNotification(
            context: Context,
            style: NotificationCompat.Style,
            channelID: String,
            room: RoomFull,
            priority: String
    ): NotificationCompat.Builder {
        val roomImage = ImageLoader(context, GlideApp.with(context)).getRoomImageAsBitmap(room.getDialogPhoto(), room)

        val notificationRaw = NotificationCompat.Builder(context, channelID)
                .setStyle(style)
                .setSmallIcon(R.drawable.ic_notification_logo)
                .setDeleteIntent(createDeleteIntent(context, room.room?.id!!))
                .setContentIntent(createTapIntent(context, room.room?.id!!))
                .setAutoCancel(true)
                .setGroup("blog.nordgedanken.simplematrix.rooms")
                .setLargeIcon(roomImage)

        if (priority == "normal") {
            notificationRaw.priority = NotificationCompat.PRIORITY_DEFAULT
            notificationRaw.setLights(ContextCompat.getColor(context, R.color.secondaryColor), 5000, 5000)
        } else if (priority == "high") {
            notificationRaw.priority = NotificationCompat.PRIORITY_HIGH
            notificationRaw.color = ContextCompat.getColor(context, R.color.primary_red)
            notificationRaw.setLights(ContextCompat.getColor(context, R.color.primary_red), 4000, 4000)
        }

        return notificationRaw
    }

    private fun createTapIntent(
            context: Context,
            roomID: String
    ): PendingIntent {
        // Create an Intent for the activity you want to start
        val resultIntent = Intent(context, ChatRoom::class.java)
        resultIntent.putExtra("roomID", roomID)

        // Create the TaskStackBuilder
        return TaskStackBuilder.create(context).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(resultIntent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT)
        }
    }

    private fun createDeleteIntent(
            context: Context,
            roomID: String
    ): PendingIntent {
        val intent = Intent()
        intent.action = NOTIFICATION_DELETED_ACTION
        intent.putExtra("room_id", roomID)

        return PendingIntent.getBroadcast(
                context,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT
        )
    }

    fun createNotificationChannel(context: Context, channelID: String) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var name: String? = null
            var description: String? = null
            var importance: Int? = null
            when (channelID) {
                DirectChatsChannelID -> {
                    name = context.getString(R.string.channel_direct_name)
                    description = context.getString(R.string.channel_direct_description)
                    importance = NotificationManagerCompat.IMPORTANCE_DEFAULT
                }
                FavChatsChannelID -> {
                    name = context.getString(R.string.channel_favs_name)
                    description = context.getString(R.string.channel_favs_description)
                    importance = NotificationManagerCompat.IMPORTANCE_DEFAULT
                }
                ChatsChannelID -> {
                    name = context.getString(R.string.channel_chats_name)
                    description = context.getString(R.string.channel_chats_description)
                    importance = NotificationManagerCompat.IMPORTANCE_DEFAULT
                }
                LISTENING_FOR_EVENTS_NOTIFICATION -> {
                    name = context.getString(R.string.channel_foreground_service_notification)
                    description = context.getString(R.string.channel_foreground_service_notification_description)
                    importance = NotificationManager.IMPORTANCE_MIN
                }
            }
            val channel = NotificationChannel(channelID, name!!, importance!!)
            channel.description = description!!
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            mNotificationManager.createNotificationChannel(channel)
        }
    }

    /**
     * Build a polling thread listener notification
     *
     * @param context       Android context
     * @param subTitleResId subtitle string resource Id of the notification
     * @return the polling thread listener notification
     */
    fun buildForegroundServiceNotification(context: Context, @StringRes subTitleResId: Int): android.app.Notification {
        // build the pending intent go to the home screen if this is clicked.
        val i = Intent(context, RoomsActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pi = PendingIntent.getActivity(context, 0, i, 0)

        val builder = NotificationCompat.Builder(context, LISTENING_FOR_EVENTS_NOTIFICATION)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(context.getString(R.string.default_app_name))
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentText(context.getString(subTitleResId))
                .setSmallIcon(R.drawable.ic_notification_logo)
                .setContentIntent(pi)
                .setColor(ContextCompat.getColor(context, R.color.secondaryColor))


        val notification = builder.build()

        notification.flags = notification.flags or NotificationCompat.FLAG_NO_CLEAR

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // some devices crash if this field is not set
            // even if it is deprecated

            // setLatestEventInfo() is deprecated on Android M, so we try to use
            // reflection at runtime, to avoid compiler error: "Cannot resolve method.."
            try {
                val deprecatedMethod = notification.javaClass
                        .getMethod("setLatestEventInfo", Context::class.java, CharSequence::class.java, CharSequence::class.java, PendingIntent::class.java)
                deprecatedMethod.invoke(notification, context, context.getString(R.string.default_app_name), context.getString(subTitleResId), pi)
            } catch (ex: Exception) {
                Logger.e("buildNotification(): Exception - setLatestEventInfo() Msg=" + ex.message, ex)
            }

        }

        return notification
    }
}