package blog.nordgedanken.simplematrix.utils.ui;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Progressbar that just calls the default implementation of Progressbar
 * Should always be used instead of {@link android.widget.ProgressBar}
 */
public class ProgressBar extends android.widget.ProgressBar {

  public ProgressBar(Context context) {
    super(context);
  }

  public ProgressBar(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }
}