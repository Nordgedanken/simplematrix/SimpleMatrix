package blog.nordgedanken.simplematrix.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import blog.nordgedanken.simplematrix.NOTIFICATION_DELETED_ACTION
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import org.jetbrains.anko.doAsync
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject


/**
 * Created by MTRNord on 24.02.2019.
 */
class NotificationBroadcastReceiver : BroadcastReceiver(), KoinComponent {
    private val db: AppDatabase by inject()

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action

        if (action == null || action != NOTIFICATION_DELETED_ACTION) {
            return
        }
        val roomID = intent.extras?.getString("room_id")
        doAsync {
            db.notificationDao().deleteNotificationByRoomID(roomID!!)
        }

    }
}