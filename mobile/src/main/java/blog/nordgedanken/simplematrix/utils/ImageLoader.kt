package blog.nordgedanken.simplematrix.utils

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import androidx.core.graphics.drawable.toBitmap
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.data.view.User
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.runOnUiThread
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

/**
 * Handles different types of Image Loading
 *
 * @author Marcel Radzio
 *
 */
class ImageLoader(val context: Context, val glide: RequestManager) : KoinComponent {
    private val account: Account by inject()

    /**
     * Loads images for entities of type [User].
     * This happens async
     *
     * @param imageView The target where the image should get loaded into.
     *
     * @param url The MXC URL from the Matrix Network that should get loaded into the imageView
     *
     * @param user The [User] entity
     *
     */
    fun loadImage(imageView: ImageView, url: String, user: User) {
        doAsync {
            val name = user.name

            var requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()

            if (user.nameAvatar == null) {
                user.nameAvatar = getNameAvatar(name)
            }

            requestOptions = requestOptions.placeholder(user.nameAvatar)

            if (url.startsWith("mxc://")) {
                val future = glide.asBitmap()
                        .load(account.client?.getMedia(url)?.permaLink?.toString())
                        .apply(requestOptions)
                        .submit()

                val bitmap = future.get()
                context.runOnUiThread {
                    imageView.setImageBitmap(bitmap)
                }
            } else {
                context.runOnUiThread {
                    imageView.setImageDrawable(user.nameAvatar)
                }
            }
        }
    }

    /**
     * Loads images for entities of type [RoomFull].
     * This happens async
     *
     * @param imageView The target where the image should get loaded into.
     *
     * @param url The MXC URL from the Matrix Network that should get loaded into the imageView
     *
     * @param dialog The [RoomFull] entity
     *
     */
    fun loadImage(imageView: ImageView, url: String, dialog: RoomFull) {
        doAsync {
            val name = dialog.getDialogName()
            var requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()

            if (dialog.room?.nameAvatar == null) {
                dialog.room?.nameAvatar = getNameAvatar(name)
            }


            requestOptions = requestOptions.placeholder(dialog.room?.nameAvatar)

            if (url.startsWith("mxc://")) {
                val future = glide.asBitmap()
                        .load(account.client?.getMedia(url)?.permaLink?.toString())
                        .apply(requestOptions)
                        .submit()

                val bitmap = future.get()
                context.runOnUiThread {
                    imageView.setImageBitmap(bitmap)
                }
            } else {
                context.runOnUiThread {
                    imageView.setImageDrawable(dialog.room?.nameAvatar)
                }
            }
        }
    }

    /**
     * Loads images from a MXC URL.
     * This happens async
     *
     * @param imageView The target where the image should get loaded into.
     *
     * @param url The MXC URL from the Matrix Network that should get loaded into the imageView
     *
     */
    fun loadImage(imageView: ImageView, url: String) {
        doAsync {
            if (url.startsWith("mxc://")) {
                val requestOptions = RequestOptions()
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transforms(RoundedCorners(10))
                val future = glide.asBitmap()
                        .load(account.client?.getMedia(url)?.permaLink?.toString())
                        .apply(requestOptions)
                        .submit()

                val bitmap = future.get()
                context.runOnUiThread {
                    imageView.setImageBitmap(bitmap)
                }
            }
        }
    }

    /**
     * Generates a generic [NameAvatar] based on either the RoomID, UserID or alias.
     *
     * @param name The Name of either a Room or User
     *
     */
    private fun getNameAvatar(name: String): NameAvatar {
        return if (name.count() > 1 && name.startsWith("!")) {
            NameAvatar(context, name.trimStart('!')[0].toString().toUpperCase())
        } else if (name.count() > 1 && name.startsWith("@")) {
            NameAvatar(context, name.trimStart('@')[0].toString().toUpperCase())
        } else if (name.count() > 1 && name.startsWith("#")) {
            NameAvatar(context, name.trimStart('#')[0].toString().toUpperCase())
        } else if (name.count() > 1) {
            NameAvatar(context, name[0].toString().toUpperCase())
        } else {
            NameAvatar(context, "")
        }
    }

    fun getRoomImageAsBitmap(url: String, dialog: RoomFull): Bitmap {
        val name = dialog.getDialogName()
        var requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()

        if (dialog.room?.nameAvatar == null) {
            dialog.room?.nameAvatar = getNameAvatar(name)
        }


        requestOptions = requestOptions.placeholder(dialog.room?.nameAvatar)

        return if (url.startsWith("mxc://")) {
            glide.asBitmap().load(account.client?.getMedia(url)?.permaLink?.toString())
                    .apply(requestOptions)
                    .submit()
                    .get()
        } else {
            dialog.room?.nameAvatar?.toBitmap()!!
        }
    }

}