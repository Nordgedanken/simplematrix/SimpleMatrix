/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.db

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import blog.nordgedanken.simplematrix.data.matrix.Sync
import blog.nordgedanken.simplematrix.data.matrix.SyncDao
import blog.nordgedanken.simplematrix.data.view.*
import blog.nordgedanken.simplematrix.utils.converters.Converters
import com.orhanobut.logger.Logger

/**
 * Created by MTRNord on 01.09.2018.
 */
@Database(
        entities = [
            Message::class,
            blog.nordgedanken.simplematrix.data.view.Room::class,
            User::class,
            Sync::class,
            Notification::class,
            AccountData::class
        ],
        version = 16
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun messageDao(): MessageDao
    abstract fun roomDao(): RoomDao
    abstract fun userDao(): UserDao
    abstract fun syncDao(): SyncDao
    abstract fun notificationDao(): NotificationDao
    abstract fun accountDataDao(): AccountDataDao

    companion object {
        /**
         * The only instance
         */
        private var sInstance: AppDatabase? = null

        private val MIGRATION_9_10 = object : Migration(9, 10) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // BROKEN! SKIP!
                /*database.execSQL("CREATE INDEX rooms_name ON rooms (dialogName)")
                database.execSQL("CREATE INDEX rooms_notification ON rooms (notificationID)")
                database.execSQL("CREATE INDEX rooms_tag ON rooms (tag)")*/
            }
        }

        private val MIGRATION_10_11 = object : Migration(10, 11) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // BROKEN! SKIP!
                /*database.execSQL("CREATE INDEX index_rooms_id_dialogName ON rooms (id, dialogName)")
                database.execSQL("CREATE INDEX index_rooms_id_notificationID ON rooms (id, notificationID)")
                database.execSQL("CREATE INDEX index_rooms_id_tag ON rooms (id, tag)")*/
            }
        }

        private val MIGRATION_11_12 = object : Migration(11, 12) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE rooms ADD COLUMN prevBatchToken TEXT")
            }
        }

        /**
         * Gets the singleton instance of SampleDatabase.
         *
         * @param context The context.
         * @return The singleton instance of AppDatabase.
         */
        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (sInstance == null) {
                sInstance = Room
                        .databaseBuilder(context.applicationContext, AppDatabase::class.java, "cache")
                        .fallbackToDestructiveMigration()
                        //.addMigrations(MIGRATION_9_10, MIGRATION_10_11, MIGRATION_11_12)
                        .build()
            }
            return sInstance!!
        }
    }

    // Helper
    fun doesRoomExistInDB(roomID: String): Boolean {
        return this.roomDao().exists(roomID)
    }

    fun removeRoomFromDB(roomID: String) {
        this.roomDao().deleteByID(roomID)
    }

    /**
     *
     * Saves an [ArrayList] of [RoomFull] to the Database.
     * It also handles to check if they are new Rooms or existing ones.
     *
     */
    fun saveRoomToDB(rooms: List<RoomFull>) {
        val workMapDB = this.roomDao().allIDs()
        val new: Iterable<blog.nordgedanken.simplematrix.data.view.Room> = rooms.asSequence().filter { it.room?.id !in workMapDB }.mapNotNull { it.room }.asIterable()
        val update: Iterable<blog.nordgedanken.simplematrix.data.view.Room> = rooms.asSequence().filter { it.room?.id in workMapDB }.mapNotNull { it.room }.asIterable()
        if (new.firstOrNull() != null) this.roomDao().insertAll(new)
        if (update.firstOrNull() != null) this.roomDao().updateAll(update)
    }

    /**
     *
     * Saves an [List] of [User] to the Database.
     * It also handles to check if they are new Users or existing ones.
     *
     */
    fun saveUserToDB(users: List<User>) {
        val new: Iterable<User> = users.asSequence().filter { it.id == null }.asIterable()
        val update: Iterable<User> = users.asSequence().filter { it.id != null }.asIterable()
        if (new.firstOrNull() != null) this.userDao().insertAll(new)
        if (update.firstOrNull() != null) this.userDao().updateAll(update)
    }

    /**
     *
     * Saves an [List] of [MessageFull] to the Database.
     * It also handles to check if they are new Messages or existing ones.
     *
     */
    fun saveMessagesToDB(messages: List<MessageFull>) {
        val workMapDB = this.messageDao().allIDs()
        val new: Iterable<Message> = messages.asSequence().filter { it.message?.id !in workMapDB }.mapNotNull { it.message }.asIterable()
        val update: Iterable<Message> = messages.asSequence().filter { it.message?.id in workMapDB }.mapNotNull { it.message }.asIterable()

        try {
            this.messageDao().insertAll(new)
        } catch (e: SQLiteConstraintException) {
            Logger.e("Failed to save new Message to DB: ${e.message}")
        }

        try {
            this.messageDao().updateAll(update)
        } catch (e: SQLiteConstraintException) {
            Logger.e("Failed to update Message to DB: ${e.message}")
        }

    }

    fun getMessagesByRoomIDFromDB(roomID: String): List<MessageFull?> {
        return this.messageDao().loadAllByRoomId(roomID)
    }


    fun getMessageCountByRoomIDFromDB(roomID: String): Int {
        return this.messageDao().countByRoomID(roomID)
    }

    /**
     * Changes the ID of a Message in the Database
     */
    fun updateMessageIDInDB(oldID: String, newID: String) {
        this.messageDao().updateMessageID(oldID, newID)
    }

    /**
     * Changes the sending status of a Message in the Database
     */
    fun updateMessageSendingStatusInDB(id: String, status: Boolean) {
        this.messageDao().updateMessageSendingStatus(id, status)
    }

    /**
     *
     * Checks if a room does already exist in the DB
     *
     * @return A [Boolean] indicating if it exists
     *
     */
    fun roomExistsInCache(roomID: String): Boolean {
        var exists = false
        val room = this.roomDao().roomIDByID(roomID)
        if (room == roomID) {
            exists = true
        }
        return exists
    }

    /**
     *
     * Gets a room by it's ID
     *
     * @return [RoomFull]
     *
     */
    fun getRoomByID(roomID: String): RoomFull? {
        return this.roomDao().roomByID(roomID)
    }

    /**
     *
     * Gets Users from the cache.
     * @return a [List] of [User]
     *
     */
    fun getUsersFromCache(): List<User> {
        return this.userDao().all()
    }

    /**
     *
     * Gets Users from the cache.
     * @return a [List] of [User]
     *
     */
    fun getUserByMXIDRoomIDFromCache(userID: String, roomID: String): User? {
        return this.userDao().userForMessage(roomID, userID)
    }

    /**
     *
     * Gets Users from the cache by the attached roomID.
     * @return a [List] of [User]
     */
    fun getUsersByRoomIDFromCache(roomID: String): List<User> {
        return this.userDao().allByRooom(roomID)
    }

    /**
     *
     * Gets Users from the cache by the attached roomID and mxid.
     * @return a [List] of [User]
     */
    fun getUsersByRoomIDAndMXIDFromCache(roomID: String, mxid: String): List<User> {
        return this.userDao().allByRoomAndMXID(roomID, mxid)
    }
}