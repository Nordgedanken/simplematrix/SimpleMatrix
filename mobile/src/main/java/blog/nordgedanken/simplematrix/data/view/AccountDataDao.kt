package blog.nordgedanken.simplematrix.data.view

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import blog.nordgedanken.simplematrix.utils.db.BaseDao

/**
 * Created by MTRNord on 27.02.2019.
 */
@Dao
abstract class AccountDataDao : BaseDao<AccountData> {
    @Transaction
    @Query("SELECT * FROM account_data")
    abstract fun all(): List<AccountData>

    @Query("DELETE FROM account_data")
    abstract fun nukeTable()
}