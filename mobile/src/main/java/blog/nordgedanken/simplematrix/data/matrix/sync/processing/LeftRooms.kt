package blog.nordgedanken.simplematrix.data.matrix.sync.processing

import blog.nordgedanken.simplematrix.data.db.AppDatabase
import io.kamax.matrix.client._SyncData
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

/**
 * LeftRooms processes any left rooms from the Sync
 *
 * @author  Marcel Radzio
 * @since   2018-10-28
 *
 * @param data Data means all the data of left rooms which came using the Sync
 * and needs to be a [Set] of type [io.kamax.matrix.client._SyncData.LeftRoom]
 */
class LeftRooms(data: Set<_SyncData.LeftRoom>) : KoinComponent {
    private val db: AppDatabase by inject()

    /**
     * init is the entry of this class and gets used to split the data into work chunks
     */
    init {
        for (chunk in data) {
            db.removeRoomFromDB(chunk.id)
        }
    }
}