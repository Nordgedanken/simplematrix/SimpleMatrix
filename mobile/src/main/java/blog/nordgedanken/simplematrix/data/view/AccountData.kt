package blog.nordgedanken.simplematrix.data.view

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by MTRNord on 27.02.2019.
 */
@Entity(tableName = "account_data")
class AccountData {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}