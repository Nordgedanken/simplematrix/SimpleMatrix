package blog.nordgedanken.simplematrix.data.matrix.sync.backgroundSync

import android.app.IntentService
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.App
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.matrix.Sync
import blog.nordgedanken.simplematrix.data.matrix.sync.SyncStarter
import blog.nordgedanken.simplematrix.data.matrix.sync.processing.SyncProcessing
import blog.nordgedanken.simplematrix.utils.Connectivity
import blog.nordgedanken.simplematrix.utils.Notification
import blog.nordgedanken.simplematrix.utils.forceNewLogin
import com.orhanobut.logger.Logger
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.client.regular.SyncOptions
import org.koin.android.ext.android.inject


/**
 * Created by MTRNord on 15.02.2019.
 */
class BackgroundSyncService : IntentService("BG_Sync_Service") {

    private val ONGOING_NOTIFICATION_ID = 9999

    private val account: Account by inject()
    private val db: AppDatabase by inject()
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate() {
        super.onCreate()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        val notification = Notification().buildForegroundServiceNotification(this, R.string.backgroundSync)

        this.startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    override fun onHandleIntent(intent: Intent?) {
        loop()
    }

    private fun sync(timeout: Long, nextBatchToken: String): _SyncData? {
        val jsonString = account.backgroundSyncFilter
        return account.client?.sync(SyncOptions.Builder().setSince(nextBatchToken).setTimeout(timeout).setFilter(jsonString).get())
    }

    // TODO return enum
    private fun actualWork(db: AppDatabase): Boolean {
        // FIXME: NEEDED?
        val localInitialSyncDone = SyncStarter.initialSyncDone.value ?: true
        if (!localInitialSyncDone) {
            Logger.d("InitialSync not done")
            return true
        }

        // Get Sync Token from DB
        Logger.d("nextup Sync")
        val all = db.syncDao().all
        if (all.isEmpty()) return true
        val nextBatchToken = all[0].syncToken

        val syncData = try {
            sync(3000, nextBatchToken)
        } catch (e: MatrixClientRequestException) {
            if (e.error.isPresent) {
                val error = e.error.get()
                if (error.errcode == "M_UNKNOWN_TOKEN") {
                    forceNewLogin(this)
                    return false
                }
                Logger.d("ErrorCode: " + error.errcode + "\nErrorMessage: " + error.error)
            }
            null
        } catch (e: Exception) {
            Logger.d("Some error happened: ${e.message}")
            null
        }

        if (syncData !== null) {
            SyncProcessing(syncData)
            // Save the next Batch token for the next sync iteration
            if (db.syncDao().all.count() == 1) {
                val sync = db.syncDao().all[0]
                val token = syncData.nextBatchToken()!!
                sync.syncToken = token
                db.syncDao().updateOne(sync)
            } else {
                val sync = Sync()
                val token = syncData.nextBatchToken()!!
                sync.syncToken = token
                db.syncDao().insertOne(sync)
            }
            return false
        } else {
            return false
        }
    }

    private fun loop() {
        Logger.d("Doing background sync")
        while (true) {
            if (Connectivity.isConnected(this)) {
                val critical = actualWork(db)
                if (critical) {
                    Logger.e("Critical Error")
                    stopSelf()
                }
                // TODO delay longer in case of empty data
                if (App.appState == "background") {
                    val minutesS = sharedPreferences.getString("bg_sync_time", "5")
                    val minutes = minutesS?.toLong()!!
                    Thread.sleep(minutes * 1000 * 60) // 5min
                } else {
                    val minutesS = sharedPreferences.getString("fg_sync_time", "1")
                    val minutes = minutesS?.toLong()!!
                    Thread.sleep(minutes * 1000 * 60) // 1min
                }
            }
        }
    }
}