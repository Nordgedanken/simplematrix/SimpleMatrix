package blog.nordgedanken.simplematrix.data.matrix.sync.initialSync

import android.app.IntentService
import android.content.Intent
import android.view.View
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.matrix.Sync
import blog.nordgedanken.simplematrix.data.matrix.sync.SyncStarter
import blog.nordgedanken.simplematrix.data.matrix.sync.processing.SyncProcessing
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import blog.nordgedanken.simplematrix.utils.Connectivity
import blog.nordgedanken.simplematrix.utils.Notification
import blog.nordgedanken.simplematrix.utils.forceNewLogin
import com.google.gson.JsonParseException
import com.orhanobut.logger.Logger
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.client.regular.SyncOptions
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import org.koin.android.ext.android.inject

/**
 * Created by MTRNord on 15.02.2019.
 */
class InitialSyncService : IntentService("Initial_Sync_Service") {
    private val ONGOING_NOTIFICATION_ID = 9998

    private val account: Account by inject()
    private val db: AppDatabase by inject()

    private enum class SyncErrors {
        FatalError,
        SomethingWentWrong,
        Empty,
        Finished,
    }

    fun sync(timeout: Long): _SyncData? {
        val jsonString = account.initialSyncFilter
        return account.client?.sync(SyncOptions.build().setTimeout(timeout).setFilter(jsonString).get())
    }

    override fun onCreate() {
        super.onCreate()

        val notification = Notification().buildForegroundServiceNotification(this, R.string.initialSync)

        this.startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    override fun onHandleIntent(intent: Intent?) {
        loop()
    }

    private fun loop() {
        RoomsActivity.loadProgressVisibility.postValue(View.VISIBLE)
        RoomsActivity.syncStatusVisibility.postValue(View.VISIBLE)
        RoomsActivity.syncStatus.postValue(getString(R.string.starting_initial_sync))

        var retries = 0
        sync@ while (true) {
            if (Connectivity.isConnected(this)) {
                Logger.d("Starting sync, retries: $retries")
                RoomsActivity.syncStatus.postValue(getString(R.string.starting_initial_sync_with_retries, retries))
                val status = actualWork()
                when (status) {
                    SyncErrors.FatalError -> stopSelf()
                    SyncErrors.SomethingWentWrong -> stopSelf()
                    SyncErrors.Finished -> break@sync
                    else -> retries++
                }

                // No delay!
            }
        }
        SyncStarter.initialSyncDone.postValue(true)

        RoomsActivity.loadProgressVisibility.postValue(View.GONE)
        RoomsActivity.syncStatusVisibility.postValue(View.GONE)
        SyncStarter.startSync(this)
        stopSelf()
    }

    private fun actualWork(): SyncErrors {
        Sentry.getContext().recordBreadcrumb(
                BreadcrumbBuilder().setMessage("Initial Sync").build()
        )

        // Try initial Sync
        val syncData = try {
            sync(0)
        } catch (e: MatrixClientRequestException) {
            if (e.error.isPresent) {
                val error = e.error.get()
                if (error.errcode == "M_UNKNOWN_TOKEN") {
                    forceNewLogin(this)
                    return SyncErrors.FatalError
                }
                Logger.d("ErrorCode: " + error.errcode + "\nErrorMessage: " + error.error)
            }
            null
        } catch (e: JsonParseException) {
            null
        }

        return if (syncData != null) {
            processSyncResponse(syncData)
        } else {
            Logger.d("No response!")
            SyncErrors.Empty
        }
    }

    private fun processSyncResponse(syncData: _SyncData): SyncErrors {
        // Save the next Batch token for the next sync iteration
        if (db.syncDao().all.count() == 1) {
            val sync = db.syncDao().all[0]
            val token = syncData.nextBatchToken()!!
            sync.syncToken = token
            db.syncDao().updateOne(sync)
        } else {
            val sync = Sync()
            val token = syncData.nextBatchToken()!!
            sync.syncToken = token
            db.syncDao().insertOne(sync)
        }

        RoomsActivity.syncStatus.postValue(getString(R.string.processing_initial_sync_response))

        SyncProcessing(syncData)
        return SyncErrors.Finished
    }

}