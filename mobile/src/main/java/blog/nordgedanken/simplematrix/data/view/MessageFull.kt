package blog.nordgedanken.simplematrix.data.view

import androidx.room.Embedded
import androidx.room.Relation
import com.orhanobut.logger.Logger
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import java.util.*

/**
 * Created by MTRNord on 15.10.2018.
 */
class MessageFull {
    @Embedded
    var message: Message? = null

    @Relation(parentColumn = "user_id",
            entityColumn = "mxid")
    var usersDB: List<User> = ArrayList()

    @Transient var userDB: User? = null

  fun getUser(): User {
        if (this.userDB == null) {
            if (this.usersDB.isEmpty()) {
                Sentry.getContext().recordBreadcrumb(
                        BreadcrumbBuilder().setMessage("[MESSAGE FULL] USERS EMPTY").build()
                )
                Logger.e("USERS EMPTY")
            }
            for (user in usersDB) {
                val roomID = message?.roomID
                if (roomID !== null) {
                    if (user.roomID == roomID) {
                        this.userDB = user
                        break
                    }
                }
            }
        }
        if (this.userDB == null) {
          Logger.e("Missing user: ${message?.userID}  MessageText: ${message?.text}  MessageHTMLText: ${message?.textHTML}  MessageID: ${this.message?.id}")
        }
        return this.userDB!!
    }
}