/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.view

import androidx.core.app.Person
import androidx.core.graphics.drawable.IconCompat
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.utils.NameAvatar
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

@Entity(tableName = "users")
class User : KoinComponent {
    @delegate:Ignore
    private val account: Account by inject()

    enum class Membership(val value: String) {
        JOIN("join"),
        LEAVE("leave"),
        BAN("ban"),
        INVITE("invite")
    }


    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    @ColumnInfo(name = "mxid")
    var MXID: String? = null
    @ColumnInfo(name = "room_id")
    var roomID: String? = null
    @ColumnInfo(name = "name")
    var name: String = ""
    @ColumnInfo(name = "avatar")
    var avatar: String = ""
    @ColumnInfo(name = "isOnline")
    var isOnline: Boolean? = false
    @ColumnInfo(name = "membership")
    var membership: Membership? = null
    @Ignore
    var nameAvatar: NameAvatar? = null

    fun getPerson(): Person {
        val person = Person.Builder().setBot(false)
                .setName(name)
        if (avatar.startsWith("mxc://")) {
            person.setIcon(IconCompat.createWithContentUri(account.client?.getMedia(avatar)?.permaLink?.toString()))
        }
        return person
                .build()
    }

}