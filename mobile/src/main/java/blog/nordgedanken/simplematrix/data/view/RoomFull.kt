package blog.nordgedanken.simplematrix.data.view

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import blog.nordgedanken.matrix_android_sdk.Matrix
import blog.nordgedanken.matrix_android_sdk.account.Account
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.util.*

class RoomFull : KoinComponent {
    @delegate:Ignore
    private val account: Account by inject()

    @Embedded
    var room: Room? = null

    @Relation(parentColumn = "id",
            entityColumn = "room_id")
    var messages: List<Message> = ArrayList()

    @Relation(parentColumn = "id",
            entityColumn = "room_id")
    var usersDB: List<User> = ArrayList()

    fun getDialogPhoto(): String {
        if (room?.dialogPhoto?.isEmpty()!! && usersDB.isNotEmpty() && usersDB.count() <= 2) {
            val directPartnerAvatar = getDirectPartnerAvatar()
            if (directPartnerAvatar != "" && directPartnerAvatar != null) {
                return directPartnerAvatar
            }
        }
        return room?.dialogPhoto!!
    }

    private fun getDirectPartnerAvatar(): String? {
        if (account.status == Matrix.State.LoggedIn ||
                account.status == Matrix.State.BackgroundSync ||
                account.status == Matrix.State.InitialSync) {
            for (user in usersDB) {
                if (user.MXID != account.client?.user?.get()?.id) {
                    return user.avatar
                }
            }
        }

        return null
    }

    fun getUnreadCount(): Int {
        return room?.unreadCount!!
    }

    fun getUsers(): MutableList<User> {
        return usersDB.toMutableList()
    }

    fun getLastMessage(): Message? {
        if (messages.isNotEmpty()) {
            return messages.sortedBy { it.createdAt?.time }[0]
        }
        return null
    }

    fun getDialogName(): String {
        if (room?.dialogName?.isNotEmpty()!!) {
            return room?.dialogName!!
        } else if (room?.canonicalAlias?.isNotEmpty()!!) {
            return room?.canonicalAlias!!
        } else if (usersDB.isNotEmpty()) {
            val directPartnerName = getDirectPartnerName()
            if (directPartnerName != null) {
                return directPartnerName
            }
        }
        return room?.id!!
    }


    private fun getDirectPartnerName(): String? {
        if (account.status == Matrix.State.LoggedIn ||
                account.status == Matrix.State.BackgroundSync ||
                account.status == Matrix.State.InitialSync) {
            for (user in usersDB) {
                if (user.MXID != account.client?.user?.get()?.id) {
                    return user.name
                }
            }
        }
        return null
    }
}