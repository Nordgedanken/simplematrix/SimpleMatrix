package blog.nordgedanken.simplematrix.data.view

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by MTRNord on 23.02.2019.
 */
@Entity(tableName = "notifications")
data class Notification(
        @PrimaryKey @NonNull var id: Int,
        @ColumnInfo(name = "room_id") var roomID: String,
        @ColumnInfo(name = "messageIDs") var messageIDs: MutableList<String>
)