package blog.nordgedanken.simplematrix.data.matrix.sync.processing

import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.view.Room
import blog.nordgedanken.simplematrix.data.view.RoomFull
import com.orhanobut.logger.Logger
import io.kamax.matrix.client._SyncData
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

/**
 * The SyncProcessing class handles the processing from
 * Matrix format into SimpleMatrix format as well as
 * adding it to the Database
 *
 * @author  Marcel Radzio
 * @since   2018-10-28
 *
 * @param data Data means all the Data which came using the Sync
 * and needs to be of type [io.kamax.matrix.client._SyncData]
 */
class SyncProcessing(data: _SyncData) : KoinComponent{
    private val db: AppDatabase by inject()

    /**
     * This checks if either of the possible sync streams exists and than delegates the chunk
     * to the specific processing class
     */
    init {
        val joined = data.rooms.joined
        if (joined.isNotEmpty()) {
            // generate room if needed to ensure it exists in the DB before processing events
            val rooms = mutableListOf<RoomFull>()
            for (room in joined) {
                val roomId = room.id
                if (!db.roomExistsInCache(roomId)) {
                    val roomPOJO = RoomFull()
                    roomPOJO.room = Room()
                    roomPOJO.room?.id = roomId
                    rooms.add(roomPOJO)
                }
            }
            db.saveRoomToDB(rooms)
            JoinedRooms(joined)
        }

        Logger.d("Processing Something")

        if (data.rooms.invited.isNotEmpty()) {
            InvitedRooms(data.rooms.invited)
        }

        if (data.rooms.left.isNotEmpty()) {
            LeftRooms(data.rooms.left)
        }

        if (data.accountData.events.isNotEmpty()) {
            GlobalAccountData(data.accountData.events)
        }
    }

}