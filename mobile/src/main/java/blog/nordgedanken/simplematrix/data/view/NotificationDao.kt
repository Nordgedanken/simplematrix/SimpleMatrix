package blog.nordgedanken.simplematrix.data.view

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import blog.nordgedanken.simplematrix.utils.db.BaseDao

/**
 * Created by MTRNord on 23.02.2019.
 */
@Dao
abstract class NotificationDao : BaseDao<Notification> {
    @Transaction
    @Query("SELECT * FROM notifications")
    abstract fun all(): List<Notification>

    @Transaction
    @Query("SELECT * FROM notifications WHERE room_id = :roomId LIMIT 1")
    abstract fun notificationByRoomID(roomId: String): Notification?


    @Transaction
    @Query("DELETE FROM notifications WHERE room_id = :roomId")
    abstract fun deleteNotificationByRoomID(roomId: String)

    @Transaction
    @Query("DELETE FROM notifications")
    abstract fun nukeTable()
}