/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.matrix

import android.content.Context
import android.view.View
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.matrix.sync.SyncStarter
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import io.kamax.matrix._MatrixUser
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import org.jetbrains.anko.runOnUiThread
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject


class MatrixClient : KoinComponent {
    private val db: AppDatabase by inject()
    private val account: Account by inject()

    companion object {
        var name: String? = null
        var avatar_url: String? = null
    }

    fun loadCache(context: Context) {
        Sentry.getContext().recordBreadcrumb(
                BreadcrumbBuilder().setMessage("Started loading cache").build()
        )

        // Setup user if empty
        val knownUser = account.client?.user?.get()
        val user = db.getUsersFromCache().firstOrNull { user -> user.MXID == knownUser?.id }
        name = user?.name
        avatar_url = user?.avatar
        var mxUser: _MatrixUser? = null
        if (name == null || avatar_url == null || name?.isEmpty() == true || avatar_url?.isEmpty() == true) {
            mxUser = account.client?.getUser(knownUser)
        }
        if (name == null || name?.isEmpty() == true) {
            name = mxUser?.name?.get()!!
        }
        if (avatar_url == null || avatar_url?.isEmpty() == true) {
            avatar_url = mxUser?.avatarUrl?.orElse("")!!
        }

        val roomsCount = AppDatabase.getInstance(context).roomDao().allCount()

        if (roomsCount == 0) {
            SyncStarter.startInitialSync(context)
        } else {
            context.runOnUiThread {
                RoomsActivity.syncStatusVisibility.value = View.GONE
                RoomsActivity.loadProgressVisibility.value = View.GONE
                SyncStarter.initialSyncDone.value = true
            }
            Sentry.getContext().recordBreadcrumb(
                    BreadcrumbBuilder().setMessage("Cache loading finished").build()
            )
            SyncStarter.startSync(context)
        }
    }
}