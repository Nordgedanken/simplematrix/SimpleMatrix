package blog.nordgedanken.simplematrix.data.matrix.additionaltypes.accountData

import com.google.gson.JsonObject
import io.kamax.matrix.json.GsonUtil
import io.kamax.matrix.json.event.MatrixJsonEphemeralEvent

class MatrixJsonPushrulesEvent(obj: JsonObject) : MatrixJsonEphemeralEvent(obj) {
    private val content = obj.getAsJsonObject("content")

    val global = GsonUtil.findObj(this.content, "global")
}