package blog.nordgedanken.simplematrix.di

import android.accounts.Account
import android.accounts.AccountManager
import androidx.core.app.NotificationManagerCompat
import blog.nordgedanken.simplematrix.State
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.view.MessageViewModel
import blog.nordgedanken.simplematrix.data.view.RoomViewModel
import blog.nordgedanken.simplematrix.roomView.tabFragments.ChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.DirectChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.FavsFragment
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * Created by MTRNord on 04.02.2019.
 */
class AppModule {
    val definition = module {

        // Fragments
        single {
            ChatsFragment()
        }

        single {
            FavsFragment()
        }

        single {
            DirectChatsFragment()
        }

        single {
            AccountManager.get(androidApplication())
        }

        factory<Array<Account>> {
            get<AccountManager>().getAccountsByType("org.matrix")
        }

        single {
            State
        }

        single {
            AppDatabase.getInstance(androidApplication())
        }

        single<NotificationManagerCompat> {
            NotificationManagerCompat.from(androidApplication())
        }

        viewModel {
            RoomViewModel(androidApplication())
        }

        viewModel {
            MessageViewModel(androidApplication())
        }
    }
}