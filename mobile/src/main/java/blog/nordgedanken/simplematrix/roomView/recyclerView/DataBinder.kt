package blog.nordgedanken.simplematrix.roomView.recyclerView

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("backgroundImageUrl")
fun loadImage(layout: ImageView, drawable: Drawable?) {
    if (drawable != null) {
        layout.setImageDrawable(drawable)
    }
}