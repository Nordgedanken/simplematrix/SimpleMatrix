package blog.nordgedanken.simplematrix.roomView.recyclerView.items

import android.text.Spanned
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import blog.nordgedanken.simplematrix.GlideApp
import blog.nordgedanken.simplematrix.GlideRequests
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.utils.ImageLoader
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.hdodenhof.circleimageview.CircleImageView


/**
 * Created by MTRNord on 19.02.2019.
 */
@EpoxyModelClass(layout = R.layout.room_layout_element)
abstract class RoomLayoutElement : EpoxyModelWithHolder<RoomLayoutElement.Holder>() {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: View.OnClickListener? = null

    var roomImageURL: String? = null
    var room: RoomFull? = null
    var dialogName: String? = null
    var lastMessage: Spanned? = null
    var dialogDate: String? = null

    override fun bind(holder: Holder) {
        holder.layout.setOnClickListener(clickListener)
        if (roomImageURL != null && room != null) {
            holder.imageLoader.loadImage(holder.roomImage, roomImageURL!!, room!!)
        }
        room = null


        setPrecomputedText(holder.dialogName, dialogName)
        setPrecomputedText(holder.lastMessage, lastMessage)
        setPrecomputedText(holder.dialogDate, dialogDate)
    }

    private fun setPrecomputedText(view: TextView, text: CharSequence?) {
        if (text != null) {
            val params = TextViewCompat.getTextMetricsParams(view)
            (view as AppCompatTextView).setTextFuture(
                    PrecomputedTextCompat.getTextFuture(text, params, null))
        }
    }

    class Holder : EpoxyHolder() {
        lateinit var glide: GlideRequests
        lateinit var imageLoader: ImageLoader

        lateinit var layout: ConstraintLayout
        lateinit var roomImage: CircleImageView
        lateinit var dialogName: TextView
        lateinit var lastMessage: TextView
        lateinit var dialogDate: TextView

        override fun bindView(itemView: View) {

            glide = GlideApp.with(itemView)
            imageLoader = ImageLoader(itemView.context, glide)

            layout = itemView.findViewById(R.id.layout)
            roomImage = itemView.findViewById(R.id.roomImageID)
            dialogName = itemView.findViewById(R.id.dialogNameID)
            lastMessage = itemView.findViewById(R.id.lastMessageID)
            dialogDate = itemView.findViewById(R.id.dialogDate)
        }
    }
}