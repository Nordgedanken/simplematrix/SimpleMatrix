package blog.nordgedanken.simplematrix.roomView.recyclerView

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyRecyclerView

/**
 * Created by MTRNord on 27.02.2019.
 */
class RoomList : EpoxyRecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun createLayoutManager(): LayoutManager {
        val layoutParams = layoutParams

        if (layoutParams.height == RecyclerView.LayoutParams.MATCH_PARENT
                // 0 represents matching constraints in a LinearLayout or ConstraintLayout
                || layoutParams.height == 0) {

            if (layoutParams.width == RecyclerView.LayoutParams.MATCH_PARENT || layoutParams.width == 0) {
                // If we are filling as much space as possible then we usually are fixed size
                setHasFixedSize(true)
            }

            // A sane default is a vertically scrolling linear layout
            return object : LinearLayoutManager(context) {
                override fun supportsPredictiveItemAnimations(): Boolean {
                    return false
                }
            }
        } else {
            // This is usually the case for horizontally scrolling carousels and should be a sane
            // default
            return object : LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false){
                override fun supportsPredictiveItemAnimations(): Boolean {
                    return false
                }
            }
        }
    }
}