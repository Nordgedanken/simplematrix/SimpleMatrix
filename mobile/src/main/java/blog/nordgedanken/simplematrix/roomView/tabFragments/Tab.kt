package blog.nordgedanken.simplematrix.roomView.tabFragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.view.RoomViewModel
import blog.nordgedanken.simplematrix.roomView.recyclerView.PagingRoomController
import blog.nordgedanken.simplematrix.roomView.recyclerView.RoomList
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * Created by MTRNord on 11.10.2018.
 */
abstract class Tab : Fragment() {
    var recyclerView: RoomList? = null
    protected val pagingController: PagingRoomController by lazy { PagingRoomController(context!!) }

    protected val viewModel: RoomViewModel by sharedViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    protected fun initAdapter(view: View) {
        recyclerView = view.findViewById(R.id.recycler_view)

        recyclerView?.layoutManager = LinearLayoutManager(context!!)
        recyclerView?.setController(pagingController)
    }

    fun search(searchValue: String) {
        pagingController.filterValue = searchValue
        pagingController.requestForcedModelBuild()
    }
}