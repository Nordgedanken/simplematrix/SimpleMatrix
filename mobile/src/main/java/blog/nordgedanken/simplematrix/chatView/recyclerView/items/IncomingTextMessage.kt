package blog.nordgedanken.simplematrix.chatView.recyclerView.items

import android.text.Spanned
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import blog.nordgedanken.simplematrix.GlideApp
import blog.nordgedanken.simplematrix.GlideRequests
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.view.User
import blog.nordgedanken.simplematrix.utils.ImageLoader
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by MTRNord on 19.02.2019.
 */
@EpoxyModelClass(layout = R.layout.incoming_text_message_layout_element)
abstract class IncomingTextMessage : EpoxyModelWithHolder<IncomingTextMessage.Holder>() {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: View.OnClickListener? = null

    var avatarImageURL: String? = null
    var user: User? = null
    var content: Spanned? = null
    var username: String? = null
    var createdAt: Date? = null

    override fun bind(holder: Holder) {
        if (holder.avatarImage != null) {
            holder.avatarImage?.setOnClickListener(clickListener)
            if (avatarImageURL != null && user != null && holder.imageLoader != null) {
                holder.imageLoader?.loadImage(holder.avatarImage!!, avatarImageURL!!, user!!)
            }
        }
        user = null

        if (holder.content != null) {
            setPrecomputedText(holder.content!!, content!!)
        }
        if (holder.content != null) {
            setPrecomputedText(holder.username!!, username!!)
        }
    }

    private fun setPrecomputedText(view: TextView, text: CharSequence?) {
        if (text != null) {
            val params = TextViewCompat.getTextMetricsParams(view)
            (view as AppCompatTextView).setTextFuture(
                    PrecomputedTextCompat.getTextFuture(text, params, null))
        }
    }

    class Holder : EpoxyHolder() {

        var glide: GlideRequests? = null
        var imageLoader: ImageLoader? = null

        var avatarImage: CircleImageView? = null
        var content: TextView? = null
        var username: TextView? = null

        override fun bindView(itemView: View) {
            glide = GlideApp.with(itemView)
            imageLoader = ImageLoader(itemView.context, glide!!)

            avatarImage = itemView.findViewById(R.id.circleImageView)
            content = itemView.findViewById(R.id.content)
            username = itemView.findViewById(R.id.username)

        }

    }
}