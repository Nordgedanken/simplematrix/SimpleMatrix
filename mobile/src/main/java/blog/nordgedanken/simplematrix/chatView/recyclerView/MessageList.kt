package blog.nordgedanken.simplematrix.chatView.recyclerView

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView


/**
 * Created by MTRNord on 26.12.2018.
 */
class MessageList : EpoxyRecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun createLayoutManager(): LayoutManager {
        setHasFixedSize(true)

        // A sane default is a vertically scrolling linear layout
        val llm = object : LinearLayoutManager(context) {
            override fun supportsPredictiveItemAnimations(): Boolean {
                return false
            }
        }
        llm.reverseLayout = true
        llm.stackFromEnd = false
        return llm
    }
}