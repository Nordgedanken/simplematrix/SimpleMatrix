package blog.nordgedanken.simplematrix.chatView.recyclerView.items

import android.view.View
import blog.nordgedanken.simplematrix.R
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder

/**
 * Created by MTRNord on 28.02.2019.
 */
@EpoxyModelClass(layout = R.layout.placeholder_item)
abstract class PlaceholderMessage : EpoxyModelWithHolder<PlaceholderMessage.Holder>() {

    class Holder : EpoxyHolder() {

        override fun bindView(itemView: View) {
        }

    }
}