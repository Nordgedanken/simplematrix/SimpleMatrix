package blog.nordgedanken.simplematrix.chatView.recyclerView.items

import android.text.Spanned
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import blog.nordgedanken.simplematrix.GlideApp
import blog.nordgedanken.simplematrix.GlideRequests
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.utils.ImageLoader
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.epoxy.VisibilityState
import com.github.ybq.android.spinkit.SpinKitView
import java.util.*

/**
 * Created by MTRNord on 19.02.2019.
 */
@EpoxyModelClass(layout = R.layout.outgoing_image_message_layout_element)
abstract class OutgoingImageMessage : EpoxyModelWithHolder<OutgoingImageMessage.Holder>() {

    var content: Spanned? = null
    var createdAt: Date? = null

    var imageURL: String? = null

    @VisibilityState.Visibility
    var sending: Int? = null

    override fun bind(holder: Holder) {
        if (sending != null && holder.spinner != null) {
            holder.spinner?.visibility = sending!!
        }

        if (imageURL != null && holder.imageLoader != null && holder.image != null) {
            holder.imageLoader?.loadImage(holder.image!!, imageURL!!)
        }
        if (content != null && holder.image != null) {
            holder.image?.contentDescription = content
        }

        if (holder.content != null) {
            setPrecomputedText(holder.content!!, content!!)
        }
    }

    private fun setPrecomputedText(view: TextView, text: CharSequence?) {
        if (text != null) {
            val params = TextViewCompat.getTextMetricsParams(view)
            (view as AppCompatTextView).setTextFuture(
                    PrecomputedTextCompat.getTextFuture(text, params, null))
        }
    }

    class Holder : EpoxyHolder() {

        var glide: GlideRequests? = null
        var imageLoader: ImageLoader? = null

        var content: TextView? = null
        var image: ImageView? = null
        var spinner: SpinKitView? = null

        override fun bindView(itemView: View) {
            glide = GlideApp.with(itemView)
            imageLoader = ImageLoader(itemView.context, glide!!)

            content = itemView.findViewById(R.id.content)
            image = itemView.findViewById(R.id.image)
            spinner = itemView.findViewById(R.id.spin_kit)

        }

    }
}