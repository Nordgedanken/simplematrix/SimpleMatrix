package blog.nordgedanken.simplematrix.chatView.recyclerView

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.content.ContextCompat
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.UserProfile
import blog.nordgedanken.simplematrix.chatView.recyclerView.items.*
import blog.nordgedanken.simplematrix.data.view.MessageFull
import blog.nordgedanken.simplematrix.utils.sha512
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagedListEpoxyController
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.util.*
import java.util.concurrent.TimeUnit

class PagingMessageController(val context: Context) : PagedListEpoxyController<MessageFull>(
        modelBuildingHandler = Handler(Looper.getMainLooper()),
        diffingHandler = Handler(Looper.getMainLooper())
), KoinComponent {
    private val account: Account by inject()

    override fun buildItemModel(currentPosition: Int, item: MessageFull?): EpoxyModel<*> {
        if (
                item == null || item?.message == null ||
                item?.message?.id == null ||
                item?.message?.roomID == null ||
                item?.message?.id == "" ||
                item?.message?.roomID == ""
        ) {
            return PlaceholderMessage_()
                    .id(-1 * currentPosition)
        }
        val id = item?.message?.id + item?.message?.roomID
        return if (item?.message?.userID == account.id) {
            if (item?.message?.image == "") {
                val element = OutgoingTextMessage_()
                        .id(id.sha512())

                element.content = if (item.message?.getTextHTMLAsSpanned(context) != null) item.message?.getTextHTMLAsSpanned(context) else item.message?.getTextAsSpanned()
                element.sending = if (item.message?.sendingInProcess!!) View.VISIBLE else View.GONE

                element
            } else {
                val element = OutgoingImageMessage_()
                        .id(id.sha512())

                element.content = if (item?.message?.getTextHTMLAsSpanned(context) != null) item.message?.getTextHTMLAsSpanned(context) else item?.message?.getTextAsSpanned()
                element.createdAt = item?.message?.createdAt
                element.imageURL = item?.message?.image

                element
            }
        } else {
            if (item?.message?.image == "") {
                val element = IncomingTextMessage_()
                        .id(id.sha512())
                        .clickListener { _ ->
                            Sentry.getContext().recordBreadcrumb(
                                    BreadcrumbBuilder().setMessage("User is going to open a User Details").build()
                            )
                            val intent = Intent(context, UserProfile::class.java).putExtra("MXID", item.getUser().MXID).putExtra("roomID", item.message?.roomID)
                            // Swap without transition
                            ContextCompat.startActivity(context, intent,
                                    null)
                        }

                element.avatarImageURL = item.getUser().avatar
                element.content = if (item.message?.getTextHTMLAsSpanned(context) != null) item.message?.getTextHTMLAsSpanned(context) else item.message?.getTextAsSpanned()
                element.createdAt = item.message?.createdAt
                element.user = item.getUser()
                element.username = item.getUser().name

                element
            } else {
                val element = IncomingImageMessage_()
                        .id(id.sha512())
                        .clickListener { _ ->
                            Sentry.getContext().recordBreadcrumb(
                                    BreadcrumbBuilder().setMessage("User is going to open a User Details").build()
                            )
                            val intent = Intent(context, UserProfile::class.java).putExtra("MXID", item?.getUser()?.MXID).putExtra("roomID", item?.message?.roomID)
                            // Swap without transition
                            ContextCompat.startActivity(context, intent,
                                    null)
                        }

                element.avatarImageURL = item?.getUser()?.avatar
                element.content = if (item?.message?.getTextHTMLAsSpanned(context) != null) item.message?.getTextHTMLAsSpanned(context) else item?.message?.getTextAsSpanned()
                element.createdAt = item?.message?.createdAt
                element.user = item?.getUser()
                element.username = item?.getUser()?.name
                element.imageURL = item?.message?.image

                element
            }
        }
    }

    private fun getPreviousDate(previousModel: EpoxyModel<*>): Date? {
        return when (previousModel) {
            is OutgoingTextMessage_ -> {
                if (previousModel.createdAt != null) {
                    previousModel.createdAt!!
                } else {
                    null
                }
            }
            is IncomingTextMessage_ -> {
                if (previousModel.createdAt != null) {
                    previousModel.createdAt!!
                } else {
                    null
                }
            }
            is OutgoingImageMessage_ -> {
                if (previousModel.createdAt != null) {
                    previousModel.createdAt!!
                } else {
                    null
                }
            }
            is IncomingImageMessage_ -> {
                if (previousModel.createdAt != null) {
                    previousModel.createdAt!!
                } else {
                    null
                }
            }
            else -> null
        }
    }

    private fun getDateDiff(date1: Date, date2: Date, timeUnit: TimeUnit): Long {
        val diffInMillis = date2.time - date1.time
        return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS)
    }

    override fun addModels(models: List<EpoxyModel<*>>) {
        val modelsWithDateDividers = mutableListOf<EpoxyModel<*>>()
        for ((index, model) in models.withIndex()) {
            if (index == 0) {
                modelsWithDateDividers.add(model)
                continue
            }
            when (model) {
                is OutgoingTextMessage_ -> {
                    val previousModel = models[index - 1]
                    val previousDate: Date? = getPreviousDate(previousModel)
                    if (previousDate != null) {
                        if (model.createdAt != null) {
                            val diff = getDateDiff(model.createdAt!!, previousDate, TimeUnit.DAYS)
                            if (diff > 0) {
                                val dateDivider = DateDividerElement_()
                                dateDivider.id(model.createdAt?.time)
                                dateDivider.date = model.createdAt!!
                                modelsWithDateDividers.add(dateDivider)
                            }
                            modelsWithDateDividers.add(model)
                        } else {
                            modelsWithDateDividers.add(model)
                        }
                    } else {
                        modelsWithDateDividers.add(model)
                    }
                }
                is IncomingTextMessage_ -> {
                    val previousModel = models[index - 1]
                    val previousDate: Date? = getPreviousDate(previousModel)
                    if (previousDate != null) {
                        if (model.createdAt != null) {
                            val diff = getDateDiff(model.createdAt!!, previousDate, TimeUnit.DAYS)
                            if (diff > 0) {
                                val dateDivider = DateDividerElement_()
                                dateDivider.id(model.createdAt?.time)
                                dateDivider.date = model.createdAt!!
                                modelsWithDateDividers.add(dateDivider)
                            }
                            modelsWithDateDividers.add(model)
                        } else {
                            modelsWithDateDividers.add(model)
                        }
                    } else {
                        modelsWithDateDividers.add(model)
                    }
                }
                is OutgoingImageMessage_ -> {
                    val previousModel = models[index - 1]
                    val previousDate: Date? = getPreviousDate(previousModel)
                    if (previousDate != null) {
                        if (model.createdAt != null) {
                            val diff = getDateDiff(model.createdAt!!, previousDate, TimeUnit.DAYS)
                            if (diff > 0) {
                                val dateDivider = DateDividerElement_()
                                dateDivider.id(model.createdAt?.time)
                                dateDivider.date = model.createdAt!!
                                modelsWithDateDividers.add(dateDivider)
                            }
                            modelsWithDateDividers.add(model)
                        } else {
                            modelsWithDateDividers.add(model)
                        }
                    } else {
                        modelsWithDateDividers.add(model)
                    }
                }
                is IncomingImageMessage_ -> {
                    val previousModel = models[index - 1]
                    val previousDate: Date? = getPreviousDate(previousModel)
                    if (previousDate != null) {
                        if (model.createdAt != null) {
                            val diff = getDateDiff(model.createdAt!!, previousDate, TimeUnit.DAYS)
                            if (diff > 0) {
                                val dateDivider = DateDividerElement_()
                                dateDivider.id(model.createdAt?.time)
                                dateDivider.date = model.createdAt!!
                                modelsWithDateDividers.add(dateDivider)
                            }
                            modelsWithDateDividers.add(model)
                        } else {
                            modelsWithDateDividers.add(model)
                        }
                    } else {
                        modelsWithDateDividers.add(model)
                    }
                }
            }
        }
        super.addModels(modelsWithDateDividers)
    }

    init {
        isDebugLoggingEnabled = true
        setFilterDuplicates(true)
    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        if (!exception.message?.contains("Duplicate")!!) {
            throw exception
        }
    }
}