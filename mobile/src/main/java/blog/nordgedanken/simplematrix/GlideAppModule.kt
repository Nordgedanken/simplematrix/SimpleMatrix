package blog.nordgedanken.simplematrix

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by MTRNord on 06.01.2019.
 */
@GlideModule
class GlideAppModule : AppGlideModule()