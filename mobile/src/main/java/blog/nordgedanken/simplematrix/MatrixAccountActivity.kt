/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

import android.accounts.Account
import android.accounts.AccountAuthenticatorActivity
import android.accounts.AccountManager
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ProgressBar
import androidx.constraintlayout.widget.ConstraintLayout
import blog.nordgedanken.matrix_android_sdk.Matrix
import blog.nordgedanken.matrix_android_sdk.authentication.LoginResult
import blog.nordgedanken.simplematrix.accounts.MatrixAccountAuthenticator
import blog.nordgedanken.simplematrix.fcm.FCMHelper
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.orhanobut.logger.Logger
import io.kamax.matrix.MatrixID
import org.koin.android.ext.android.inject

/**
 *
 * Used to display the login UI and handling the login.
 *
 * @author Marcel Radzio
 *
 */
class MatrixAccountActivity : AccountAuthenticatorActivity() {
    private val account: blog.nordgedanken.matrix_android_sdk.account.Account by inject()

    private var accountManager: AccountManager? = null

    private val REQ_REGISTER = 11

    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.account_login)

        accountManager = AccountManager.get(baseContext)
    }

    /**
     * This function is the callback used on the Login Button
     */
    fun login(view: View?) {
        // Reset errors
        findViewById<TextInputLayout>(R.id.localpartInputLayout).error = null
        findViewById<TextInputLayout>(R.id.homeserverurlInputLayout).error = null
        findViewById<TextInputLayout>(R.id.passwordInputLayout).error = null
        findViewById<TextInputLayout>(R.id.serverInputLayout).error = null
        findViewById<Button>(R.id.sign_in_button).error = null

        // Get references of all fields
        val localpart = findViewById<TextInputEditText>(R.id.localpart).text.toString()
        val homeserverurl = findViewById<TextInputEditText>(R.id.homeserverurl).text.toString()
        val passWd = findViewById<TextInputEditText>(R.id.password).text.toString()
        val server = findViewById<TextInputEditText>(R.id.server).text.toString()

        // Get Account Type
        val accountType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE)

        // Do the login Async
        class LoginTask : AsyncTask<Void, Void, Intent?>() {
            lateinit var matrixStatus: Matrix.State

            override fun doInBackground(vararg params: Void): Intent? {
                /** Get a _MatrixID object from the supplied localpart and homeserverurl
                 *
                 * The homeserver is allowed to be empty
                 */
                val mxid = MatrixID.from(localpart, homeserverurl).acceptable()

                /**
                 * Try the actual Matrix login and pass the returned [Pair] into a variable
                 */
                val matrix = if (server.isEmpty()) {
                    // TODO properly do this when actually implementing the new SDK
                    Matrix(mxid, passWd) {
                        if (BuildConfig.ALLOW_FCM_USE) {
                            FCMHelper().init(this@MatrixAccountActivity)
                        }
                    }
                } else {
                    // TODO properly do this when actually implementing the new SDK
                    Matrix(mxid, server, passWd) {
                        if (BuildConfig.ALLOW_FCM_USE) {
                            FCMHelper().init(this@MatrixAccountActivity)
                        }
                    }
                }


                Logger.d("[Login] Login request done")

                /**
                 * Login status is kept in the second part of the Pair.
                 * The first Part holds the returned auth token.
                 */
                matrixStatus = matrix.status

                /**
                 * Match the status code of type [LoginResult]
                 */
                when (matrixStatus) {
                    // Login worked fine
                    Matrix.State.LoggedIn -> {
                        Logger.d("[Login] Success Login")

                        /**
                         * We need to save any data in a [Bundle] to access it in onPostExecute
                         */
                        val data = Bundle()
                        val user = account.client?.context?.user?.get()
                        data.putString(AccountManager.KEY_ACCOUNT_NAME, user?.localPart)
                        data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType)
                        data.putString(MatrixAccountAuthenticator.TOKEN_TYPE, "token")
                        data.putString(AccountManager.KEY_AUTHTOKEN, account.client?.accessToken?.get())
                        data.putString(MatrixAccountAuthenticator.SERVER, user?.domain)

                        val result = Intent()
                        result.putExtras(data)

                        return result
                    }


                    Matrix.State.Error -> {
                        when (matrix.errorReason!!) {
                            // We were unable to find the Server. An Error gets displayed
                            LoginResult.ServerNotFound -> {
                                Logger.d("[Login] Server not found: $matrixStatus")
                                runOnUiThread {
                                    findViewById<ConstraintLayout>(R.id.login_form).visibility = View.VISIBLE
                                    findViewById<ProgressBar>(R.id.login_progress).visibility = View.GONE
                                    val textField = findViewById<TextInputLayout>(R.id.serverInputLayout)
                                    textField.requestFocus()
                                    textField.error = this@MatrixAccountActivity.getString(R.string.server_not_found_error)
                                }
                            }

                            // Something went wrong but we have no Idea what exactly.
                            LoginResult.UnknownError -> {
                                Logger.d("[Login] Real unknown error")
                                runOnUiThread {
                                    findViewById<ConstraintLayout>(R.id.login_form).visibility = View.VISIBLE
                                    findViewById<ProgressBar>(R.id.login_progress).visibility = View.GONE
                                    val button = findViewById<Button>(R.id.sign_in_button)
                                    button.requestFocus()
                                    button.error = this@MatrixAccountActivity.getString(R.string.unknown_error)
                                }
                            }

                            // The supplied User either does not exist or the Password typed in is wrong
                            LoginResult.WrongPasswordOrUserNotFound -> {
                                Logger.d("[Login] Wrong Password")
                                runOnUiThread {
                                    findViewById<ConstraintLayout>(R.id.login_form).visibility = View.VISIBLE
                                    findViewById<ProgressBar>(R.id.login_progress).visibility = View.GONE
                                    val textField = findViewById<TextInputLayout>(R.id.passwordInputLayout)
                                    textField.requestFocus()
                                    textField.error = this@MatrixAccountActivity.getString(R.string.wrong_password_or_user_not_found)
                                }
                            }

                            // The Server returned no .well-known url.
                            // This might mean that the server does not support .well-known
                            LoginResult.WellKnownURLNotFound -> {
                                Logger.d("[Login] No Server found using well-known discovery")
                                runOnUiThread {
                                    findViewById<ConstraintLayout>(R.id.login_form).visibility = View.VISIBLE
                                    findViewById<ProgressBar>(R.id.login_progress).visibility = View.GONE
                                    val textField = findViewById<TextInputLayout>(R.id.homeserverurlInputLayout)
                                    textField.requestFocus()
                                    textField.error = this@MatrixAccountActivity.getString(R.string.homeserver_base_url_not_autodiscoverable)
                                }
                            }

                            // Else we have something we were not able to handle. Treat it as a Unknown Error
                            else -> {
                                Logger.d("[Login] Unknown Error")
                                runOnUiThread {
                                    findViewById<ConstraintLayout>(R.id.login_form).visibility = View.VISIBLE
                                    findViewById<ProgressBar>(R.id.login_progress).visibility = View.GONE
                                    val button = findViewById<Button>(R.id.sign_in_button)
                                    button.requestFocus()
                                    button.error = this@MatrixAccountActivity.getString(R.string.unknown_error)
                                }
                            }
                        }
                    }

                    // Else we have something we were not able to handle. Treat it as a Unknown Error
                    else -> {
                        Logger.d("[Login] Unknown Error")
                        runOnUiThread {
                            findViewById<ConstraintLayout>(R.id.login_form).visibility = View.VISIBLE
                            findViewById<ProgressBar>(R.id.login_progress).visibility = View.GONE
                            val button = findViewById<Button>(R.id.sign_in_button)
                            button.requestFocus()
                            button.error = this@MatrixAccountActivity.getString(R.string.unknown_error)
                        }
                    }
                }

                return null
            }

            /**
             * If the loginError is of type [Matrix.State   .LoggedIn] and we got an [Intent]
             * we can move on and process the Login Result
             */
            override fun onPostExecute(intent: Intent?) {
                if (matrixStatus == Matrix.State.LoggedIn && intent != null) {
                    setLoginResult(intent)
                }
            }
        }

        // First hide the Keyboard
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

        // Hide the Form and display a loading circle
        findViewById<ConstraintLayout>(R.id.login_form).visibility = View.GONE
        findViewById<ProgressBar>(R.id.login_progress).visibility = View.VISIBLE

        //Start the background login Task
        LoginTask().execute()
    }

    /**
     * Creates a new Account and saves it to the Android system
     */
    private fun setLoginResult(intent: Intent) {
        Logger.d("setLoginResult")

        val userId = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)

        val androidAccount = Account(userId, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE))

        if (getIntent().getBooleanExtra(MatrixAccountAuthenticator.ADD_ACCOUNT, false)) {
            val authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)
            val tokenType = intent.getStringExtra(MatrixAccountAuthenticator.TOKEN_TYPE)

            val data = Bundle()
            data.putString("localpart", account.client?.context?.user?.get()?.localPart)
            data.putString("baseUrl", account.client?.context?.hsBaseUrl.toString())
            data.putString("domain", account.client?.context?.user?.get()?.domain)
            data.putString("idString", account.client?.context?.user?.get()?.id)

            accountManager!!.addAccountExplicitly(androidAccount, "", data)
            accountManager!!.setAuthToken(androidAccount, tokenType, authtoken)
        } else {
            accountManager!!.setPassword(androidAccount, "")
        }

        setAccountAuthenticatorResult(intent.extras)
        setResult(Activity.RESULT_OK, intent)

        finish()
    }

    /**
     * Handle the Activity Result
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQ_REGISTER) {
            setLoginResult(data)
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Logger.d("The Login got aborted")
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}