package blog.nordgedanken.simplematrix.fcm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Data implements Serializable {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("format")
    @Expose
    private String format;
    private final static long serialVersionUID = 5197296951642284245L;

    /**
     * No args constructor for use in serialization
     */
    public Data() {
    }

    /**
     * @param format
     * @param url
     */
    public Data(String url, String format) {
        super();
        this.url = url;
        this.format = format;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("url", url).append("format", format).toString();
    }

}