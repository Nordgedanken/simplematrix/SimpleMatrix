package blog.nordgedanken.simplematrix.fcm

import android.content.Context
import blog.nordgedanken.matrix_android_sdk.account.Account
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import io.kamax.matrix.json.GsonUtil
import org.jetbrains.anko.doAsync
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject


class FCMHelper : KoinComponent {
    private val account: Account by inject()

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        val data = SetPusher()
        data.lang = "en"
        data.kind = "http"
        data.appDisplayName = "SimpleMatrix"
        data.deviceDisplayName = android.os.Build.MODEL
        data.appId = "blog.nordgedanken.simplematrix"
        data.pushkey = token
        data.data = Data()
        data.data.url = "https://push.nordgedanken.de/_matrix/push/v1/notify"
        data.append = false
        val json = GsonUtil.makeObj(data)

        doAsync {
            account.client?.setPusher(json)
        }
    }

    fun init(context: Context) {
        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
            val prefs = context.getSharedPreferences("blog.nordgedanken.simplematrix.fcm", Context.MODE_PRIVATE)
            val token = prefs.getString("token", "")
            if (token != "") {
                sendRegistrationToServer(token)
            }
        }
    }
}