package blog.nordgedanken.simplematrix.fcm

import android.annotation.SuppressLint
import android.content.Context
import blog.nordgedanken.matrix_android_sdk.Matrix
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.view.Message
import blog.nordgedanken.simplematrix.data.view.MessageFull
import blog.nordgedanken.simplematrix.utils.Notification
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.orhanobut.logger.Logger
import org.koin.android.ext.android.inject
import org.koin.standalone.KoinComponent


/**
 * Created by MTRNord on 23.12.2018.
 */
class SMFirebaseMessagingService : FirebaseMessagingService(), KoinComponent {
    val account: Account by inject()
    val db: AppDatabase by inject()

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @SuppressLint("ApplySharedPref")
    override fun onNewToken(token: String?) {
        Logger.d("Refreshed token: $token")

        val prefs = getSharedPreferences("blog.nordgedanken.simplematrix.fcm", Context.MODE_PRIVATE)

        prefs.edit().putString("token", token).commit()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // Check if message contains a data payload.
        remoteMessage?.data?.isNotEmpty()?.let {

            val eventID = remoteMessage.data["id"]
            val roomID = remoteMessage.data["room_id"]
            val sender = remoteMessage.data["sender"]
            val type = remoteMessage.data["msgtype"]
            val body = remoteMessage.data["body"]
            val priority = remoteMessage.data["priority"]

            val room = db.getRoomByID(roomID!!)
            if (room != null) {
                val tag = room.room?.tag

                val channel = when (tag) {
                    "chats" -> Notification().ChatsChannelID
                    "direct" -> Notification().DirectChatsChannelID
                    "favs" -> Notification().FavChatsChannelID
                    else -> Notification().ChatsChannelID
                }

                val message = Message()
                message.id = eventID!!
                message.text = body!!
                message.roomID = roomID
                message.userID = sender

                val fullMessage = MessageFull()
                fullMessage.message = message
                val usersDB = db.getUsersByRoomIDAndMXIDFromCache(
                        message.roomID!!,
                        message.userID!!
                )
                // FIXME THIS CAN BE EMPTY
                if (usersDB.isNotEmpty()
                        &&
                    (
                        account.status == Matrix.State.LoggedIn ||
                        account.status == Matrix.State.BackgroundSync ||
                        account.status == Matrix.State.InitialSync
                    )
                ) {
                    fullMessage.usersDB = usersDB

                    Notification().addMessageToRoomNotification(this, channel, priority!!, fullMessage)
                }
            }

        }
    }

}