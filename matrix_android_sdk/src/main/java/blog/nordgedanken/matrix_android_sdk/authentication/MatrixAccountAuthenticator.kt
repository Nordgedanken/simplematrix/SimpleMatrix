package blog.nordgedanken.matrix_android_sdk.authentication

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.os.Bundle

/**
 * Created by MTRNord on 09.02.2019.
 */
class MatrixAccountAuthenticator(var context: Context) : AbstractAccountAuthenticator(context) {
    override fun getAuthTokenLabel(authTokenType: String?): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun confirmCredentials(response: AccountAuthenticatorResponse?,
                                    account: Account?,
                                    options: Bundle?): Bundle? {
        return null
    }

    override fun updateCredentials(response: AccountAuthenticatorResponse?,
                                   account: Account?,
                                   authTokenType: String?,
                                   options: Bundle?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAuthToken(response: AccountAuthenticatorResponse?,
                              account: Account?,
                              authTokenType: String?,
                              options: Bundle?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hasFeatures(response: AccountAuthenticatorResponse?,
                             account: Account?,
                             features: Array<String>?): Bundle {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun editProperties(response: AccountAuthenticatorResponse?,
                                accountType: String?): Bundle {

        throw UnsupportedOperationException()
    }

    /***
     * Ask the user to login to an existing account or to create a new account
     * @see https://matrix.org/docs/spec/client_server/r0.4.0.html#post-matrix-client-r0-login
     * @see https://matrix.org/docs/spec/client_server/r0.4.0.html#post-matrix-client-r0-register
     */
    override fun addAccount(response: AccountAuthenticatorResponse?,
                            accountType: String?,
                            authTokenType: String?,
                            requiredFeatures: Array<String>?,
                            options: Bundle?): Bundle {


        val intent = Intent(context, MatrixAccountAuthenticatorActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        return bundle
    }

}