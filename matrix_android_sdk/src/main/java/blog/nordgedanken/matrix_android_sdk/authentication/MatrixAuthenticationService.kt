package blog.nordgedanken.matrix_android_sdk.authentication

import android.app.Service
import android.content.Intent
import android.os.IBinder

/**
 * Created by MTRNord on 10.02.2019.
 */
class MatrixAuthenticationService : Service() {

    private lateinit var mAuthenticator : MatrixAccountAuthenticator

    override fun onCreate() {
        super.onCreate()
        mAuthenticator = MatrixAccountAuthenticator(this)
    }

    override fun onBind(intent: Intent): IBinder {
        return mAuthenticator.iBinder
    }
}