package blog.nordgedanken.matrix_android_sdk.authentication

/**
 * Created by MTRNord on 09.02.2019.
 */
enum class LoginResult {
    Success, UnknownError, ServerNotFound, WrongPasswordOrUserNotFound, WellKnownURLNotFound
}