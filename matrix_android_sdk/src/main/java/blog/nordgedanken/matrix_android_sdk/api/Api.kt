package blog.nordgedanken.matrix_android_sdk.api

import blog.nordgedanken.matrix_android_sdk.api.json.PublicRooms
import retrofit2.Call
import retrofit2.http.GET



/**
 * Created by MTRNord on 09.02.2019.
 */
class Api {
    interface MatrixService {
        @GET("_matrix/client/r0/publicRooms")
        fun publicRooms(): Call<PublicRooms>
    }
}