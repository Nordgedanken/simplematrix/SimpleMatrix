package blog.nordgedanken.matrix_android_sdk.api.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import blog.nordgedanken.matrix_android_sdk.api.Api
import blog.nordgedanken.matrix_android_sdk.api.json.PublicRooms
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



/**
 * Created by MTRNord on 09.02.2019.
 */
class RoomList(application: Application, private val api: Api.MatrixService) : AndroidViewModel(application) {
    private val rooms: MutableLiveData<PublicRooms> by lazy {
        MutableLiveData<PublicRooms>().also {
            loadRooms()
        }
    }

    fun getRooms(): LiveData<PublicRooms> {
        return rooms
    }

    private fun loadRooms() {
        api.publicRooms().enqueue(object : Callback<PublicRooms> {
            override fun onFailure(call: Call<PublicRooms>, t: Throwable) {
                throw t
            }

            override fun onResponse(call: Call<PublicRooms>, response: Response<PublicRooms>) {
                rooms.value = response.body()
            }

        })
    }

}