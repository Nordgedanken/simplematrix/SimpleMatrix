package blog.nordgedanken.matrix_android_sdk.account

import blog.nordgedanken.matrix_android_sdk.Matrix
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import io.kamax.matrix.client.regular.MatrixHttpClient
import io.kamax.matrix.json.GsonUtil

/**
 * Created by MTRNord on 09.02.2019.
 */
class Account {
    var status: Matrix.State = Matrix.State.Uninitialized
    var id: String = ""

    val initialSyncFilter: String by lazy {
        val timeline = JsonObject()
        timeline.add("limit", JsonPrimitive(1))

        val state = JsonObject()
        state.add("lazy_load_members", JsonPrimitive(true))

        val roomFilter = JsonObject()
        roomFilter.add("timeline", timeline)
        roomFilter.add("state", state)

        val filter = JsonObject()
        filter.add("room", roomFilter)

        GsonUtil.get().toJson(filter)
    }

    val backgroundSyncFilter: String by lazy {
        val timeline = JsonObject()
        timeline.add("limit", JsonPrimitive(20))

        val state = JsonObject()
        state.add("lazy_load_members", JsonPrimitive(true))

        val roomFilter = JsonObject()
        roomFilter.add("timeline", timeline)
        roomFilter.add("state", state)

        val filter = JsonObject()
        filter.add("room", roomFilter)

        GsonUtil.get().toJson(filter)
    }
    var client: MatrixHttpClient? = null
}